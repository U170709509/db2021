use company;
create temporary table TopSuppliers(
SupplierID int primary key,
SupplierName varchar(45), 
ProductCount int


);
insert into TopSuppliers (SupplierID, SupplierName, ProductCount)
select suppliers.SupplierID, SupplierName, count(ProductID) as ProductCount
from suppliers join products on suppliers.SupplierID = products.SupplierID
group by SupplierName
order by ProductCount desc;
select * from TopSuppliers where ProductCount > 3 order by ProductCount desc;

call check_table_exists('TopSuppliers');
drop temporary table TopSuppliers;
select * from customers;
update customers
set CustomerName="Ymng", city="kashi", Country="China"
where CustomerID=1;
select * from customers;
truncate table customers;
delete from customers;

